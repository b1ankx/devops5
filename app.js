const express = require('express');
const app = express();

app.get('/data', (req, res) => {
  res.json({ message: 'Hello, World!', timestamp: new Date() });
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, '0.0.0.0', () => {
  console.log(`Server is running on http://0.0.0.0:${PORT}`);
});
