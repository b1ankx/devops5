Этот проект представляет собой простое веб-приложение на Node.js, которое возвращает JSON-ответ. Приложение контейнеризовано с использованием Docker и разворачивается с помощью GitLab CI/CD.

## Структура проекта

```
my-json-app/
├── app.js
├── package.json
├── package-lock.json
└── Dockerfile
```

- `app.js`: Основной файл приложения.
- `package.json`: Метаданные проекта и зависимости.
- `package-lock.json`: Автоматически сгенерированный файл для фиксации версий установленных пакетов.
- `Dockerfile`: Конфигурация Docker для контейнеризации приложения.

## Предварительные требования

- Node.js
- Docker
- GitLab Runner (для CI/CD)

## Настройка

1. **Клонируйте репозиторий**:
    ```bash
    git clone https://your-repository-url.git
    cd my-json-app
    ```

2. **Установите зависимости**:
    ```bash
    npm install
    ```

3. **Запустите приложение локально**:
    ```bash
    node app.js
    ```
   Приложение будет доступно по адресу `http://localhost:3000/data`.

## Docker

1. **Соберите Docker-образ**:
    ```bash
    docker build -t my-json-app .
    ```

2. **Запустите Docker-контейнер**:
    ```bash
    docker run -p 3000:3000 my-json-app
    ```
   Приложение будет доступно по адресу `http://localhost:3000/data`.

## GitLab CI/CD

Проект включает файл `.gitlab-ci.yml` для настройки непрерывной интеграции и деплоя с использованием GitLab CI/CD.

### .gitlab-ci.yml

```yaml
stages:
  - build
  - test
  - deploy

variables:
  DOCKER_HOST: tcp://docker:2375/
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA

before_script:
  - docker info

build:
  stage: build
  script:
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
  tags:
    - docker

test:
  stage: test
  script:
    - echo "Running conditional tests..."
    # Здесь можно добавить команды для запуска тестов вашего приложения
  tags:
    - docker

deploy:
  stage: deploy
  script:
    - docker pull $IMAGE_TAG
    - docker run -d --rm -p 3000:3000 $IMAGE_TAG
  only:
    - master
  tags:
    - docker
```

- **Build stage**: Сборка и отправка Docker-образа в GitLab Registry.
- **Test stage**: Условное тестирование приложения (пока placeholder).
- **Deploy stage**: Развертывание приложения на GitLab Runner. Страница сайта должна открываться в браузере.
